<?php
namespace App\DataFixtures;

use App\Entity\Apartment;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;

class ApartmentFixtures extends Fixture
{
    public function load(ObjectManager $manager) {
        $faker = Faker\Factory::create('fr_FR');

        for ($i = 0; $i < 50; $i++) {
            $apartment = new Apartment();
            $apartment->setAddress($faker->streetAddress . ", ". $faker->city);
            $apartment->setFloor($faker->randomDigit);
            $apartment->setNbRoom($faker->randomDigitNotNull);
            $apartment->setElevator($faker->boolean);
            $manager->persist($apartment);
        }
        $manager->flush();
    }
}
<?php

namespace App\Tests;

use App\Entity\Apartment;
use Doctrine\Persistence\ObjectManager;
use PHPUnit\Framework\TestCase;

final class ApartmentTest extends TestCase
{
    public function testSetAddress(): void
    {
        $apartment = new Apartment();
        $this->assertSame(null, $apartment->getAddress());
        $newAddress = '1 Place de la Republique';
        $apartment->setAddress($newAddress);
        $this->assertSame($newAddress, $apartment->getAddress());
    }

    public function testSetFloor(): void
    {
        $apartment = new Apartment();
        $this->assertSame(null, $apartment->getFloor());
        $newFloor = 3;
        $apartment->setFloor($newFloor);
        $this->assertSame($newFloor, $apartment->getFloor());
    }

    public function testSetNbRoom(): void
    {
        $apartment = new Apartment();
        $this->assertSame(null, $apartment->getNbRoom());
        $newRoom = 3;
        $apartment->setNbRoom($newRoom);
        $this->assertSame($newRoom, $apartment->getNbRoom());
    }

    public function testSetElevator(): void
    {
        $apartment = new Apartment();
        $this->assertSame(null, $apartment->getElevator());
        $newElevator = true;
        $apartment->setElevator($newElevator);
        $this->assertSame($newElevator, $apartment->getElevator());
    }

    public function testApartmentNotInBasement(): void
    {
        $apartment = new Apartment();
        $apartment->setFloor(3);

        $this->assertGreaterThan(-1, $apartment->getFloor(), 'Un appartement ne peut être au sous-sol.');
    }

    public function testApartmentHasPieces(): void
    {
        $apartment = new Apartment();
        $apartment->setNbRoom(2);

        $this->assertGreaterThan(0, $apartment->getNbRoom(), 'Un appartement a au moins une pièce.');
    }
}
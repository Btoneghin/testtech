<?php

namespace App\Tests;

use App\Repository\ApartmentRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class ApartmentControllerTest extends WebTestCase
{
    public function testIndex(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Liste des appartements');
    }

    public function testNewApartmentPage(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/new');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Ajouter un appartement');
    }

    public function testEditApartmentPage(): void
    {
        $client = static::createClient();
        $apartmentRepository = static::$container->get(ApartmentRepository::class);
        $listApartment = $apartmentRepository->findAll();
        // Selection du dernier appartement listé (au hasard)
        $apartment = end($listApartment);

        $url = '/' . $apartment->getId() . '/edit';
        $crawler = $client->request('GET', $url);

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Modifier un appartement');
    }

    public function testEditApartmentAction(): void
    {
        $client = static::createClient();
        $apartmentRepository = static::$container->get(ApartmentRepository::class);
        $listApartment = $apartmentRepository->findAll();
        // Selection du dernier appartement listé (au hasard)
        $apartment = end($listApartment);

        $url = '/' . $apartment->getId() . '/edit';
        $crawler = $client->request('GET', $url);

        $buttonCrawlerNode = $crawler->selectButton('Modifier');
        $form = $buttonCrawlerNode->form();

        $oldValue = $form['apartment[floor]']->getValue();
        $form['apartment[floor]'] = $oldValue + 1;

        // submit the Form object
        $client->submit($form);

        $listApartment = $apartmentRepository->findAll();
        // Selection du dernier appartement listé (au hasard)
        $apartment = end($listApartment);

        $this->assertEquals($oldValue + 1, $apartment->getFloor());
    }


}
